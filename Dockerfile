FROM nerzhul/archlinux

RUN pacman -Sy --noconfirm matrix-synapse \
	&& yes | pacman -Scc \
	&& test ! -r /etc/localtime || rm /etc/localtime \
	&& ln -s /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime \
	&& mkdir -p /etc/synapse /var/lib/synapse \
	&& chown -R synapse /etc/synapse /var/lib/synapse

EXPOSE 8008
EXPOSE 8448
VOLUME /var/lib/synapse

USER synapse

WORKDIR /var/lib/synapse
RUN /usr/bin/python -m synapse.app.homeserver \
		--server-name synapse \
		--config-path /etc/synapse/homeserver.yaml \
		--generate-config \
		--report-stats=yes \
	&& rm /etc/synapse/homeserver.yaml
ADD homeserver.yaml /etc/synapse/homeserver.yaml

CMD /usr/bin/python -m synapse.app.homeserver \
		--config-path=/etc/synapse/homeserver.yaml

